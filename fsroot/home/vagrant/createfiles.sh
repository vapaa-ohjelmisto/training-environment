#!/bin/bash
# This first line tells the kernel that this executable is a shell script

# mkdir makes a directory, -p option creates parent directories if missing
mkdir -p /home/vagrant/Documents/archive

for (( counter=10; counter>0; counter-- ))
do
    # touch creates an empty file, since we are in loop it will create 
    # files called dokumentti-[number]
    touch /home/vagrant/Documents/archive/dokumentti-$counter

    # printf is a tool to print formatted text
    # \n is a formatting shortcut to print new line
    # $() will execute anythin inside the quotes as shell commands
    # > symbol will redirect output of previous command to a file, 
    #   in this case we are redirecting the creation time
    printf "Tämän tiedoston luontiaika Unix muodossa on:\n$(date +%s)\n" > /home/vagrant/Documents/archive/dokumentti-$counter
done

echo "Onnistui, lippu: 'kouvola'"